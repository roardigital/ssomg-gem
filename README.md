# Ssomg

This is the Ruby client for the SSOMG single sign on project, designed to work with Rails.


## Installation

You must be using Ruby 2.4 or higher and rails 4 or higher.

<br/>

### Adding the gem
Add this line to your application's Gemfile:

```ruby
gem 'ssomg'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install ssomg


One the gem is installed there are a few things to add in the code. 

<br/>

### Adding setup methods
In `/config/application.rb`, add the line 

```Ssomg.configure```

Be sure to do this after the necessary environment variables, to ensure the module configures correctly.
 
<br/>

### Mounting necessary routes
At the top of `/config/routes.rb`, add the line
`mount Ssomg::Engine => "auth"`
To mount the expected routes for authentication. This adds `/auth/login`, `/auth/logout` and `auth/verify` to the app, none of which have views. 


<br/>

### Controller Inheritance
Then to protect routes, make sure that you application controller inherits from `Ssomg::BaseController` ( which in turn inherits from `ActionController::Base`) : 
```
class ApplicationController < Ssomg::BaseController
  # your code here
end 
```

<br/>

### Environment
It is important to have the correct environment variables set up to get this working. If you encounter errors, please check they are set up correctly.

<br/>

## Usage

Once set up, controllers inheriting from your base controller will have the class variable `@user` set, which you can use to validate requests however you want. 

The controller provides a `protect` method to only allow access to routes if the user has a specific role set. 
There are a number of ways to do this, as shown in this example controller. 

```
class WelcomeController < ApplicationController

  # To protect the whole controller
  before_action -> { protect(["admin"]) }
  # or
  before_action -> { protect("admin") }
  # or 
  before_action -> { protect(["admin", "read"]) }


  def index
    # Or to protect the method
    protect(["admin"]); return if performed?
    # or 
    protect("admin"); return if performed?
    # or
    protect(["admin", "myrole"]); return if performed?

    @var = "hello"
  end

private 


end
```

If a user JWT is set, @user will be set like this:

```
@user => {
  "_id": "xxxxx",
  "first_name": "First",
  "last_name": "Last",
  "email": "xxxxxxxx@xxxx.xx",
  "roles": [
    "App Role 1",
    "App Role 2"
  ],
  "refresh_token": "xxxxxxxxx",
  "iat": xxxxxxxxxx,
  "exp": xxxxxxxxxx
}
```

### Environment ###

When you register the app and the users in the main SSOMG admin, you'll be issued with an app id and the public key used to verify tokens. 
You'll need to add the public key to your project, and then add the following attributes to your environment variables to ensure it works correctly: 

```
PUB_KEY_PATH=./keys/sso
APP_ID=xxxxxxxxxxxxxxxxxxxxxxxx
SSO_HOST=https://auth.mysite.com
```

You can set these with docker, or here's a boilerplate: 
`/config/application.rb`
``` 
module RoarAuthTesterRails
  class Application < Rails::Application
    .
    .
    .

    env_file = File.join(Rails.root, 'config', 'local_env.yml')
    YAML.load(File.open(env_file)).each do |key, value|
      ENV[key.to_s] = value
    end if File.exists?(env_file)

    Ssomg.configure
  end
end
```

`/config/local_env.rb`
```
APP_ID: xxxxxxxxxxxxxxxxxxxxxx
PUB_KEY_PATH: ./config/public_key
SSO_HOST: http://auth.myhost.com
```

In production, the app will automatically use secure and http-only cookies.

## Setup Gotchas

Make sure the URLs in the env file and the provider app contain the correct protocol(http(s)), and ports. Without the correct protocols, the app will behave unexpectedly.

As the controller inherits from the SSOMG engine, view helpers may not work as expected. You will need to manually add them. Some example code: 

```
// app/controllers/application_controller.rb

class ApplicationController < Ssomg::BaseController
  protect_from_forgery with: :exception
  include ApplicationHelper

  def not_found
    raise ActionController::RoutingError.new('Not Found')
  end
end



// app/helpers/application_helper.rb

module ApplicationHelper
  include AdminHelper
end

```


## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/ssomg.
