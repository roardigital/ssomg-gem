require "ssomg/version"
require "jwt"
require 'ssomg/controllers/root_controller'
require 'ssomg/controllers/base_controller'
require 'ssomg/controllers/api_controller'
require 'ssomg/controllers/auth_controller'
require 'ssomg/engine'


module Ssomg
  class Error < StandardError; end
end
