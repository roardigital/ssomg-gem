module Ssomg
  Engine.routes.draw do
    get '/login', to: 'auth#login'
    get '/verify', to: 'auth#verify'
    get '/logout', to: 'auth#logout'
  end
end
