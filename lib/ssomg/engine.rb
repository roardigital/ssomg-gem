module Ssomg
  @@PUB_KEY = ""
  class Engine < Rails::Engine

    paths["app/controllers"] = ["lib/ssomg/controllers"]
    paths["config/routes.rb"] = ["lib/ssomg/config/routes.rb"]
    isolate_namespace Ssomg
  end

  def self.configure
    @@PUB_KEY = OpenSSL::PKey::RSA.new( File.read( ENV["PUB_KEY_PATH"] ) )
  end

  def self.PUB_KEY
    @@PUB_KEY
  end

end
