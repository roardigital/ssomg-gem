module Ssomg
  class AuthController < RootController
    def verify
      verify_token    
    end

    def login
      cookies["ssomg_meta" ] = { :value => "/", :secure => Rails.env.production?, :httponly => true }
      go_to_provider
    end

    def logout
      clear_linked_cookies
      clear_cookies
      redirect_to ENV["SSO_HOST"] + "/auth/logout" and return
    end
  end
end