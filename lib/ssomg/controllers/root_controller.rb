require 'net/http'
require 'json'

module Ssomg
  class RootController < ::ActionController::Base

    before_action :register_user, unless: -> { request.query_parameters["token"] }

  private 

    def get_token
      return nil
    end

    def register_user 
      token = get_token
      if( token )
        begin 
          decoded_token = ::JWT.decode token, Ssomg.PUB_KEY, true, { algorithm: 'RS256' }
          @user = decoded_token[ 0 ]
        rescue ::JWT::ExpiredSignature
          if ( cookies["ssomg"] )
            begin
              decoded_token = ::JWT.decode token, Ssomg.PUB_KEY, true, { exp_leeway: 432000, algorithm: 'RS256' } #5 day leeway to ensure token is read
              accessTokens = JSON.parse refresh( decoded_token[ 0 ]["refresh_token"] )
              userToken = accessTokens[ENV["APP_ID"]]
              cookies["ssomg" ] = { :value => accessTokens[ENV["APP_ID"]], :secure => Rails.env.production?, :httponly => true }
              withoutMain = accessTokens.except!( ENV["APP_ID"] )
              cookies["ssomg_all" ] = { :value => withoutMain.keys.join(","), :secure => Rails.env.production?, :httponly => true }
              withoutMain.each { |key, value| 
                cookies["ssomg_" + key ] = { :value => value, :secure => Rails.env.production?, :httponly => true }
              }
              decoded_user = ::JWT.decode userToken, Ssomg.PUB_KEY, true, { algorithm: 'RS256' }
              @user = decoded_user[ 0 ]
            rescue StandardError => e
              # raise e
            end
          else 
            cookies["ssomg_meta" ] = { :value => request.original_url, :secure => Rails.env.production?, :httponly => true }
            go_to_provider 
          end
        rescue StandardError => e
          # raise e
        end
      end
    end

    def verify_token
      if request.query_parameters["token"] 
        accessTokens = JSON.parse refresh( request.query_parameters["token"] )
        cookies["ssomg" ] = { :value => accessTokens[ENV["APP_ID"]], :secure => Rails.env.production?, :httponly => true }
        withoutMain = accessTokens.except!( ENV["APP_ID"] )
        cookies["ssomg_all" ] = { :value => withoutMain.keys.join(","), :secure => Rails.env.production?, :httponly => true }
        withoutMain.each { |key, value| 
          cookies["ssomg_" + key ] = { :value => value, :secure => Rails.env.production?, :httponly => true }
        }
        if ( cookies["ssomg_meta"] ) 
          path = cookies["ssomg_meta"]
          cookies.delete "ssomg_meta"
          redirect_to path and return
        end
      end
    end

    def protect( roles )
      if ( @user )
        if !roles.kind_of?(Array)
          roles = [ roles ]
        end
        authorised = false;
        for role in roles
          if ( @user["roles"].include? role ) 
            authorised = true
            break
          end
        end
        if ( !authorised ) 
          head(403) and return      
        end
      else 
        cookies["ssomg_meta" ] = { :value => request.original_url, :secure => Rails.env.production?, :httponly => true }
        go_to_provider
      end
      
    end

    def refresh( token ) 
      begin
        uri = URI(ENV["SSO_HOST"] + "/auth/sso")
        http = Net::HTTP.new(uri.host, uri.port )
        req = Net::HTTP::Post.new(uri.path, {'Content-Type' =>'application/json'})
        if uri.scheme == "https" 
          http.verify_mode = OpenSSL::SSL::VERIFY_NONE
          http.use_ssl = true
        end
        req.body = { :token => token }.to_json
        res = http.request(req)
        jwt = res.body
        return jwt
      rescue StandardError => e
        # puts "failed #{e}"
      end
    end

    def refresh_silent token 
      begin
        uri = URI(ENV["SSO_HOST"] + "/auth/sso")
        http = Net::HTTP.new(uri.host, uri.port )
        req = Net::HTTP::Post.new(uri.path, {'Content-Type' =>'application/json'})
        if uri.scheme == "https" 
          http.verify_mode = OpenSSL::SSL::VERIFY_NONE
          http.use_ssl = true
        end
        req.body = { :token => token }.to_json
        res = http.request(req)
        jwt = res.body
        return jwt
      rescue StandardError => e
      end
    end

    def bearer_token
      pattern = /^Bearer /
      header  = request.headers['Authorization']
      header.gsub(pattern, '') if header && header.match(pattern)
    end

    def clear_linked_cookies
      if cookies["ssomg_all"] 
        all_cookies = cookies["ssomg_all"].split(",")
        all_cookies.each { |key| cookies.delete "ssomg_" + key }
        cookies.delete "ssomg_all"
      end
    end

    def clear_cookies
      cookies.delete "ssomg_meta"
      cookies.delete "ssomg"
    end

    def go_to_provider
      clear_linked_cookies
      redirect_to ENV["SSO_HOST"] + "/auth/login?app_id=" + ENV["APP_ID"] and return
    end

  end
end