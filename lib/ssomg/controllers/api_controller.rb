require 'net/http'
require 'json'

module Ssomg
  class ApiController < RootController

    before_action :register_user, unless: -> { request.query_parameters["token"] }

  private 

    def get_token 
      bearer_token
    end
   
  end
end